#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 매달 계산할때 전체 합산 총액의 판단 기준을 잡기 빡셌음.
# 본 코드는 그 기준을 정확히 제시함.
# 매달 `GREP_S' 를 갱신하여 실행할 것.
# z002.py ===> 열의 합

import subprocess
import sys

BRANCH = "karma"
FURL_BASE = "https://gitlab.com/soyeomul/hanwoo/-/raw/%s/007jch" % (BRANCH)
GREP_S = sys.argv[1] # 해당 년월을 명시하는 구분자: 2019년 4월 ===> @1904
THIS_YEAR = "20" + GREP_S[1:3]
FURL = "%s/%s.txt" % (FURL_BASE, THIS_YEAR)

#
# 파일 내려받기 함수
#
def _get_text(x, y):
    from subprocess import Popen, PIPE
    """
    `curl' 은 Linux/*BSD 등에서 유명한 도구입니다
    본 코드는 그래서 가급적 Linux/*BSD 시스템에서 실험하시길 권유드립니다
    """
    _cmd = "curl -s -f {0} | grep {1}".format(x, y)
    _try = Popen(_cmd, stdout=PIPE, shell=True)

    output = _try.communicate()[0].decode("utf-8").strip()

    return output


lines = _get_text(FURL, GREP_S).split("\n")

rst_1 = []
for x in lines:
    rst_1.append(x.split("|")[1])
a = rst_1
int_a = list(map(int, a))
sum_a = sum(int_a)

rst_2 = []
for x in lines:
    rst_2.append(x.split("|")[2])
b = rst_2
int_b = list(map(int, b))
sum_b = sum(int_b)

rst_3 = []
for x in lines:
    rst_3.append(x.split("|")[3])
c = rst_3
int_c = list(map(int, c))
sum_c = sum(int_c)

rst_4 = []
for x in lines:
    rst_4.append(x.split("|")[4])
d = rst_4
int_d = list(map(int, d))
sum_d = sum(int_d)

rst_5 = []
for x in lines:
    rst_5.append(x.split("|")[5])
e = rst_5
int_e = list(map(int, e))
sum_e = sum(int_e)

rst_6 = []
for x in lines:
    rst_6.append(x.split("|")[6])
f = rst_6
int_f = list(map(int, f))
sum_f = sum(int_f)

rst_7 = []
for x in lines:
    rst_7.append(x.split("|")[7])
g = rst_7
int_g = list(map(int, g))
sum_g = sum(int_g)

rst_8 = []
for x in lines:
    rst_8.append(x.split("|")[8])
h = rst_8
int_h = list(map(int, h))
sum_h = sum(int_h)

sum_total = sum_a + sum_b + sum_c + sum_d + sum_e + sum_f + sum_g + sum_h
list_total = [sum_a, sum_b, sum_c, sum_d, sum_e, sum_f, sum_g, sum_h]

print(sum_total)
print(list_total)

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 마지막 갱신: 2022년 4월 23일
