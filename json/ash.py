# -*- coding: utf-8 -*-

# 암소 목록 테이블
# KPN 표시 ===> 수정암소
# 4278- 표시 ===> 발정탐지기 착용 암소
# 밑소 표시 ===> 밑소 
# 후보 밑소 표시 ===> 후밑

import sys

from lex__ import _get_url # custom module

BRANCH = "karma"
FURL = "https://gitlab.com/soyeomul/hanwoo/-/raw/%s/002ash.txt" % (BRANCH)
fdata = _get_url(FURL)

lines = fdata.splitlines(False)
lines = [x for x in lines if x.startswith("F")]

수정암소 = []
탐지기착용암소 = []
밑소 = []
후밑 = []
for x in lines:
    if "KPN" in x:
        수정암소.append((x.split("|")[0])[1:6])
    if "4278-" in x:
        탐지기착용암소.append((x.split("|")[0])[1:6])
    if "밑소" in x:
        밑소.append((x.split("|")[0])[1:6])
    if "후밑" in x:
        후밑.append((x.split("|")[0])[1:6])

# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2023년 10월 15일
