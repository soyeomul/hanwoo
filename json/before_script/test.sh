#!/usr/bin/env bash
# -*- coding: utf-8 -*-

# 환경: Ubuntu 18.04 LTS
# 필요한 패키지들을 점검합니다
#
### 참고문헌:
# <<처음 배우는 셸 스크립트 -- ISBN 9791162243893>>, 45페이지

_curl="`which curl`"
_py3k="`which python3`"
_lr="`which lsb_release`"

if [ -z $_curl  ] || [ -z $_py3k ] || [ -z $_lr ]; then
    echo "===> so we have to prepare apt-get ..."
    apt-get update -qq
fi

if [ -z $_curl ]; then 
    echo "===> curl not found"
    apt-get install -y -qq curl
fi

if [ -z $_py3k ]; then
    echo "===> python3 not found"
    apt-get install -y -qq python3
fi

if [ -z $_lr ]; then
    echo "===> lsb_release not found"
    apt-get install -y -qq lsb-release
fi
   
# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2021년 12월 13일
