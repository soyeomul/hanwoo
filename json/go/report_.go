// -*- coding: utf-8 -*-
//
// 깃랩 작업 보고서 (원본: report_.py)
//
// 참고문헌:
// <가장 빨리 만나는 Go언어 (이재홍 지음) ISBN 978-89-6618-990-8>
// <https://gitlab.com/soyeomul/hanwoo/-/raw/master/json/report_.py>
// <https://stackoverflow.com/questions/35142532/how-to-split-long-lines-for-fmt-sprintf>
// <https://stackoverflow.com/questions/54269243/golang-equivalent-of-creating-a-subprocess-in-python/54269689>

package main

import "fmt"
import "os"
import "os/exec"
import _ "reflect"
import "소여물" /* $GOPATH/src/소여물 */

/***
### 전자메일 주요 환경 변수 (사용자 정의)
#
# MAIL_FROM: 보내는이 주소
# RCPT_TO: 받는이 주소
# MAIL_API_KEY: 보내는이 (메일건) API 키
# MAIL_API_ADDR: 보내는이 (메일건) API 주소
***/

func mf() string {
	var MAIL_DOMAIN = os.Getenv("THANKS_FQDN")
	var MAIL_USER = os.Getenv("THANKS_GOPHER")
	var MAIL_NAME = os.Getenv("THANKS_DOMOONSOJA")

	ss := fmt.Sprintf("%s <%s@%s>",
		MAIL_NAME,
		MAIL_USER,
		MAIL_DOMAIN,
	)

	return ss
}

func rt() string {
	var RCPT_TO = os.Getenv("THANKS_RCPT_TO")

	return RCPT_TO
}

func mak() string {
	var __MGK__ = os.Getenv("THANKS_MGK")
	ss := 소여물.SEU(__MGK__)
	rs := 소여물.HBH(ss)

	return rs
}

func maa() string {
	var MAIL_DOMAIN = os.Getenv("THANKS_FQDN")
	ss := fmt.Sprintf("%s%s%s",
		"https://api.mailgun.net/v3/",
		MAIL_DOMAIN,
		"/messages",
	)

	return ss
}

/***
### 보고서 제목 및 본문 내용 ###
#
# 내용0. 마지막 커밋
# 내용1. 커밋 변경점
# 내용2. 빌드 로그
#
# ... 관련 환경 변수 항목들 ...
#
# - CI_PROJECT_TITLE
#
# - CI_PROJECT_URL
# - CI_COMMIT_SHA
# - CI_COMMIT_MESSAGE
***/

func ms() string {
	var PROJECT_TITLE = os.Getenv("CI_PROJECT_TITLE")
	ctime := 소여물.Stime()

	ss := fmt.Sprintf("깃랩 작업 보고서 (%s) -- %s",
		PROJECT_TITLE,
		ctime,
	)

	return ss
}

func commit_url() (string, string, string) {
	PROJECT_URL := os.Getenv("CI_PROJECT_URL")
	COMMIT_SHA := os.Getenv("CI_COMMIT_SHA")
	COMMIT_MESSAGE := os.Getenv("CI_COMMIT_MESSAGE")

	url := fmt.Sprintf("%s%s%s",
		PROJECT_URL,
		"/-/commit/",
		COMMIT_SHA,
	)

	return PROJECT_URL, COMMIT_MESSAGE, url
}

func mt() string {
	x, y, z := commit_url()
	TBL := os.Getenv("THANKS_BUILD_LOG")
	TS := os.Getenv("THANKS_SIGNATURE")

	ss := fmt.Sprintf(`
... curl 메일건 api 로 보냅니다 ...

작업장 주소: %s

[0] 마지막 작업:
===> %s

[1] 작업 변경점:
===> %s

[2] 빌드 로그:
===> %s

__
%s
`,
		x,
		y,
		z,
		TBL,
		TS,
	)

	return ss
}

var MAIL_FROM = mf()
var RCPT_TO = rt()
var MAIL_API_KEY = mak()
var MAIL_API_ADDR = maa()

var MAIL_SUBJECT = ms()
var MAIL_TEXT = mt()

var curl_mailgun = fmt.Sprintf("curl -s --user 'api:%s' %s -F from='%s' -F to=%s -F subject='%s' -F text='%s'",
	MAIL_API_KEY,
	MAIL_API_ADDR,
	MAIL_FROM,
	RCPT_TO,
	MAIL_SUBJECT,
	MAIL_TEXT,
)

func main() {
	curlCmd := exec.Command("bash", "-c", curl_mailgun)
	curlOut, err := curlCmd.Output()
	if err != nil {
		panic(err)
	}
	curlCmd.Start()
	fmt.Println(string(curlOut))
}

// 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
// 마지막 갱신: 2023년 4월 5일
