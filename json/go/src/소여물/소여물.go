// -*- coding: utf-8 -*-
// 고랭지 농법: 사용자 함수
//
// 참고문헌:
// <https://golangbyexample.com/reverse-a-string-in-golang/>
// <https://stackoverflow.com/questions/54269243/golang-equivalent-of-creating-a-subprocess-in-python/54269689>

package 소여물

import "strings"
import "os/exec"

// 문자열 뒤집기
func HBH(s string) string {
	idx := len(s)

	ss := make([]string, 0, idx)
	for i := idx - 1; i >= 0; i-- {
		ss = append(ss, string(s[i]))
	}

	rs := strings.Join(ss[:], "")

	return rs
}

// 전자메일 주소 분리후 UID 획득
func SEU(s string) string {
	ss := strings.Split(s[:], "@")[0]

	return ss
}

// 시스템 시간
func Stime() string {
	dateCmd := exec.Command("date")
	dateOut, err := dateCmd.Output()
	if err != nil {
		panic(err)
	}

	ss := string(dateOut)

	rs := strings.TrimRight(ss, "\n")

	return rs
}

// 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
// 마지막 갱신: 2021년 10월 17일
