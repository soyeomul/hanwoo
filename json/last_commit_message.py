# -*- coding: utf-8 -*-

import os
import json
from subprocess import Popen, PIPE


### PROJECT_ID: 7112726 (hanwoo) ###

PROJECT_ID = os.getenv('CI_PROJECT_ID')

if PROJECT_ID == None:
    PROJECT_ID = "7112726"

### PROJECT_ID: 7112726 (hanwoo) ###


_cmd = """\
curl --silent \
-XGET 'https://gitlab.com/api/v4/projects/%s/repository/commits'""" % (
    PROJECT_ID,
)

_try = Popen(_cmd, stdout=PIPE, shell=True)

output = _try.communicate()[0].decode("utf-8").strip()

dl = json.loads(output)

_message = dl[0]['message']
#_web_url = dl[0]['web_url'].split("-")[0]

"""
이름과 전자메일은 저장소 작업디렉토리에서
 셸 명령어로 초기화 할 수 있음:
$ git config user.name "황병희"
$ git config user.email "soyeomul@gmail.com"
"""
#_author_email = dl[0]['author_email']
#_author_name = dl[0]['author_name']

# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2021년 10월 11일
