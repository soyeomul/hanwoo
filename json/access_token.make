# -*- coding: utf-8 -*-

import json

_token = [120, 120, 45, 104, 104, 117, 72, 98, 106, 113, 65, 80, 54, 117, 69, 103, 54, 117, 54, 56]

_token = dict([tuple(x) for x in enumerate(_token)])

_scopes = [
    "api",
    "read_user",
    "read_api",
    "read_repository",
    "read_registry",
]

_name = "test0329"
_created = "2021-03-29"
_expires = "2022-03-30"
_maint = "soyeomul@gmail.com"

KEY = ["_TOKEN", "_SCOPES", "_NAME", "_CREATED", "_EXPIRES", "_MAINTAINER",]
VALUE = [_token] + [_scopes] + [_name] + [_created] + [_expires] + [_maint]

dd = dict(zip(KEY, VALUE))

dj = json.dumps(dd, indent=8)

print(dj)

# 편집: VIM (Ubuntu 18.04)
# 마지막 갱신: 2021년 3월 30일
