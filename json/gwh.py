#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import re
import json
import os

from xml_ import XML_BASE_IMP # custom module
from credit import CREDIT, 도구, 갱신 # custom module
from history import HISTORY # custom module
#from thanks import __thanks__ # custom module
from betla import betla # custom module


### dotenv ###

__thanks__ = os.getenv('THANKS_TODAY')

if __thanks__ == None:
    import thanks
    __thanks__ = thanks.__thanks__

### dotenv ###


우방 = (
    "중간본동-1", "중간본동-2", "중간본동-3", "중간본동-4", "중간본동-5", "중간본동-6", "중간본동-7", "중간본동-8", "중간본동-9",
    "꼭대기동-1", "꼭대기동-2", "꼭대기동-3", "꼭대기동-4", "꼭대기동-5", "꼭대기동-6", "꼭대기동-7",
    "아랫동안-1", "아랫동안-2", "아랫동안-3", "아랫동안-4", "아랫동안-5", "아랫동안-6", "아랫동안-7", "아랫동안-8",
    "아랫동밖-1", "아랫동밖-2", "아랫동밖-3", "아랫동밖-4", "아랫동밖-5", "아랫동밖-6", "아랫동밖-7", "아랫동밖-8",
)

RJJ1 = []; RJJ2 = []; RJJ3 = []; RJJ4 = []; RJJ5 = []; RJJ6 = []; RJJ7 = []; RJJ8 = []; RJJ9 = [];
RKK1 = []; RKK2 = []; RKK3 = []; RKK4 = []; RKK5 = []; RKK6 = []; RKK7 = [];
RAA1 = []; RAA2 = []; RAA3 = []; RAA4 = []; RAA5 = []; RAA6 = []; RAA7 = []; RAA8 = [];
RAB1 = []; RAB2 = []; RAB3 = []; RAB4 = []; RAB5 = []; RAB6 = []; RAB7 = []; RAB8 = [];

for idx in range(len(XML_BASE_IMP)):
    xyz = XML_BASE_IMP[idx][1]
    abc = XML_BASE_IMP[idx][0]
    if 우방[0] in xyz:
        RJJ1.append(abc)
    if 우방[1] in xyz:
        RJJ2.append(abc)
    if 우방[2] in xyz:
        RJJ3.append(abc)
    if 우방[3] in xyz:
        RJJ4.append(abc)
    if 우방[4] in xyz:
        RJJ5.append(abc)
    if 우방[5] in xyz:
        RJJ6.append(abc)
    if 우방[6] in xyz:
        RJJ7.append(abc)
    if 우방[7] in xyz:
        RJJ8.append(abc)
    if 우방[8] in xyz:
        RJJ9.append(abc)
    if 우방[9] in xyz:
        RKK1.append(abc)
    if 우방[10] in xyz:
        RKK2.append(abc)
    if 우방[11] in xyz:
        RKK3.append(abc)
    if 우방[12] in xyz:
        RKK4.append(abc)
    if 우방[13] in xyz:
        RKK5.append(abc)
    if 우방[14] in xyz:
        RKK6.append(abc)
    if 우방[15] in xyz:
        RKK7.append(abc)
    if 우방[16] in xyz:
        RAA1.append(abc)
    if 우방[17] in xyz:
        RAA2.append(abc)
    if 우방[18] in xyz:
        RAA3.append(abc)
    if 우방[19] in xyz:
        RAA4.append(abc)
    if 우방[20] in xyz:
        RAA5.append(abc)
    if 우방[21] in xyz:
        RAA6.append(abc)
    if 우방[22] in xyz:
        RAA7.append(abc)
    if 우방[23] in xyz:
        RAA8.append(abc)
    if 우방[24] in xyz:
        RAB1.append(abc)
    if 우방[25] in xyz:
        RAB2.append(abc)
    if 우방[26] in xyz:
        RAB3.append(abc)
    if 우방[27] in xyz:
        RAB4.append(abc)
    if 우방[28] in xyz:
        RAB5.append(abc)
    if 우방[29] in xyz:
        RAB6.append(abc)
    if 우방[30] in xyz:
        RAB7.append(abc)
    if 우방[31] in xyz:
        RAB8.append(abc)


개체명부 = [
    RJJ1, RJJ2, RJJ3, RJJ4, RJJ5, RJJ6, RJJ7, RJJ8, RJJ9,
    RKK1, RKK2, RKK3, RKK4, RKK5, RKK6, RKK7,
    RAA1, RAA2, RAA3, RAA4, RAA5, RAA6, RAA7, RAA8,
    RAB1, RAB2, RAB3, RAB4, RAB5, RAB6, RAB7, RAB8,
]


def _HELP():
    sys.exit("사용법: %s %s 또는 %s %s ;;;" % (
        sys.argv[0], "--json",
        sys.argv[0], "--html",
    ))
    
    
def _2JSON(xyz):
    new_xyz = []
    for idx in xyz:
        new_idx = idx[:2] + idx[9:13] + idx[14:]
        new_xyz.append(new_idx)

    return new_xyz


def _2HTML(xyz):
    new_xyz = []
    for idx in xyz:
        if "+M" in idx[:2]:
            span_begins = "<span class='MT'>"
        elif "-M" in idx[:2]:
            span_begins = "<span class='MF'>"
        elif "+F" in idx[:2]:
            span_begins = "<span class='FT'>"
        else:
            span_begins = "<span class='FF'>"
        span_ends = "</span>"
        new_idx = "%s%s%s%s%s" % (
            idx[:9], span_begins,
            idx[9:13], span_ends,
            idx[13:],
        )
        new_xyz.append(new_idx)

    return new_xyz


새개체명부 = list(map(_2HTML, 개체명부))
우방 = dict(zip(우방, 새개체명부))

KEY = ["우방", "현황_개요", "작업_이력",]
VALUE = [우방] + [CREDIT] + [HISTORY]

dd = dict(zip(KEY, VALUE))
dj = json.dumps(dd, indent=8, ensure_ascii=False)

### HTML5 BEGINS HERE ###
dhtml = """\
<!DOCTYPE html>
<!-- -*- coding: %s -*- -->
<!-- 참고문헌(HTML5): https://www.w3.org/TR/html52/ -->

<!--

 ###
 ### 배꾸레에 얽힌 이야기
 ###

 하늘의 은혜와 덕화에 보은할때에 배꾸레가 커진다
  그래서 항상 언제나 연원께 고맙고 감사한 마음 
  그리고 천하창생의 모든 생명을 반드시 다 살리고자하는 마음으로 일하며 걸어간다
 최종 목적지는 후천의 바다 그곳은 포덕천하와 함께 완성된다 ...^^^
 
 # 편집: GNU Emacs 27.1 (Ubuntu 18.04)
 # 마지막 갱신: 2021년 4월 28일
 
-->

<html lang="ko">
<head>
  <meta charset="%s" />
  <title>%s</title>
  <meta name="Generator" content="%s" />
  <meta name="Modified" content="%s" />
  <style>
    span.FF {
      font-weight: bold;
      color: #FFCC00;
      text-decoration: underline dotted #FFCC00;
    }
    span.FT {
      font-weight: bold;
      color: #BF9900;
      text-decoration: underline #BF9900;
    }    
    span.MF {
      font-weight: bold;
      color: #8080FF;
      text-decoration: underline dotted #8080FF;
    }
    span.MT {
      font-weight: bold;
      color: #2020E0;
      text-decoration: underline #2020E0;
    }
    span.THANKS {
      font-family: serif;
      font-size: small;
      color: #838383;
      text-shadow: 1px 1px 1px #838383;
    }
    span.BETLA {
      color: #747474;
      font-family: sans-serif;
      font-size: medium;
    }
    div.DEFAULT {
      text-align: left;
    }
    div.CENTER {
      text-align: center;
    }
    hr.FOOTER {
      border: 8px solid #228B22;
      border-radius: 3px;
    }
    body {
      background-image: url("%s");
      height: %s;
      background-position: center;
      background-repeat: no-repeat;
      background-size: %s;
    }
  </style>
</head>

<body>

<!--

  ### 암소/수소 구분 색상표는 Gnus 로고에서 차용함

    (defvar gnus-logo-color-alist
      '((flame "#cc3300" "#ff2200")
        (pine "#c0cc93" "#f8ffb8")
        (moss "#a1cc93" "#d2ffb8")
        (irish "#04cc90" "#05ff97")
        (sky "#049acc" "#05deff")
        (tin "#6886cc" "#82b6ff")
        (velvet "#7c68cc" "#8c82ff")
        (grape "#b264cc" "#cf7df")
        (labia "#cc64c2" "#fd7dff")
        (berry "#cc6485" "#ff7db5")
        (dino "#724214" "#1e3f03")
        (oort "#cccccc" "#888888")
        (storm "#666699" "#99ccff")
        (pdino "#9999cc" "#99ccff")
        (purp "#9999cc" "#666699")
        (no "#ff0000" "#ffff00")
        (neutral "#b4b4b4" "#878787")
        (ma "#2020e0" "#8080ff")
        (september "#bf9900" "#ffcc00"))
      "Color alist used for the Gnus logo.")

    - 암소(F): September Gnus
    - 수소(M): Ma Gnus
  
  # 참고문헌: [0-1]
  # [0] http://git.savannah.gnu.org/cgit/emacs.git/tree/lisp/gnus/gnus.el
  # [1] Gnus 공식 홈페이지: <https://www.gnus.org>

-->
 
<div class='DEFAULT'>

<pre>
%s
</pre>

<br />
--
<br />

<!-- ^고맙습니다 _布德天下_ 감사합니다_^))// -->
<span class='THANKS' title="%s">
%s
</span>
<!-- ^고맙습니다 _布德天下_ 감사합니다_^))// -->

</div>

<div class='CENTER'>

<!-- BETLA -->
<pre>
<span class='BETLA'>
%s
</span>
</pre>
<!-- BETLA -->



<!-- 대숲농장 바닥선 색깔: #228B22 (forest green) -->
<hr class='FOOTER' />

</div>

</body>
</html>""" % (
    "utf-8",
    "utf-8",
    "대숲농장 한우 개체 위치 현황판",
    도구[6:],
    갱신[8:],
    "대숲농장.png",
    "100%",
    "83% 83%",
    dj,
    __thanks__,
    __thanks__,
    betla,
)
### HTML5 ENDS HERE ###


###
### 이제 마무리 합니다~
###

if __name__ == "__main__":
    if len(sys.argv) == 1:
        _HELP()
        
    elif sys.argv[1] == "--html":
        print(dhtml)
        
    elif sys.argv[1] == "--json":
        새개체명부 = list(map(_2JSON, 개체명부))
        KEY = 우방
        VALUE = 새개체명부
        dd = dict(zip(KEY, VALUE))
        dj = json.dumps(dd, indent=8, ensure_ascii=False)
        
        print(dj)
        
    else:
        _HELP()


# 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
# 마지막 갱신: 2021년 10월 11일
