# -*- coding: utf-8 -*-

import os

from jgh import 새식별번호 # custom module

from last_commit_message import _message # custom module
from signature import signature # custom module

from 콤푸타 import 갱신 # custom module
from 콤푸타 import 도구 # custom module
from 콤푸타 import 운영체제 # custom module
from 콤푸타 import _GITLAB_BUILD_HOSTNAME as 깃랩_콤푸타 # custom module
from 콤푸타 import 대숲농장_셈틀깨비 as 셈틀깨비 # custom module

import 경운기 # custom module

### test ###

def _email():
    _default = "<soyeomul@penguin.birch.chromebook>"
    
    lv = "&lt;"
    rv = "&gt;"
    env_addr = os.getenv('CI_COMMIT_AUTHOR')
    if env_addr == None:
        return _default
    
    html_addr = env_addr.replace("<", lv)
    html_addr = html_addr.replace(">", rv)

    return html_addr


_email = _email()
_www = os.getenv('CI_PROJECT_URL')

### test ###

변경사항 = "마지막 작업: %s" % (_message) 
두수 = "총두수: %s 두" % (len(새식별번호))
우편주소 = "대숲농장 우편주소: 경상북도 울진군 평해읍 평오곡길 213-12 [36363]"
누리터 = "대숲농장 누리터: %s" % (_www)
전자메일 = "대숲농장 전자메일: %s" % (_email)
서명 = signature

CREDIT = [
    두수, str(갱신), 변경사항, 도구, 운영체제,
    깃랩_콤푸타, 우편주소, 누리터, 전자메일, 셈틀깨비,
    경운기._, 서명,
]
CREDIT = dict([tuple(x) for x in enumerate(CREDIT, start=0)])

# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2021년 10월 9일
