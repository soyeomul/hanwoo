# -*- coding: utf-8 -*-

# 대숲농장_식별번호 만들기

import sys
import re

from lex__ import _get_url, hex_to_str # custom module
from ash import 수정암소, 탐지기착용암소, 밑소, 후밑 # custom module
from age import _mage # custom module

_ox = "0x1F402" # U+1F402
_erice = "0x1F33E" #U+1F33E
_hvoltage = "0x26A1" # U+26A1

BRANCH = "karma"
FURL = "https://gitlab.com/soyeomul/hanwoo/-/raw/%s/000jgh.txt" % (BRANCH)
fdata = _get_url(FURL)

lines = fdata.splitlines(False)
lines = [x for x in lines if x[:4].isdigit()]

배꾸레 = [] # 새식별번호로 재구성 그리고 커져라 배꾸레 ^^^
for b in lines:
    if "암소" in b:
        if b[13:18] in 수정암소:
            if b[13:18] in 탐지기착용암소:
                배꾸레.append("+F" + b[6:18] + "=" + _mage(b[27:33]))
            else:
                배꾸레.append("+F" + b[6:18] + "-" + _mage(b[27:33]))
        else:
            if b[13:18] in 탐지기착용암소:
                배꾸레.append("-F" + b[6:18] + "=" + _mage(b[27:33]))
            else:
                배꾸레.append("-F" + b[6:18] + "-" + _mage(b[27:33]))
    elif "거세" in b:
        배꾸레.append("+M" + b[6:18] + "-" + _mage(b[27:33]))
    else:
        배꾸레.append("-M" + b[6:18] + "-" + _mage(b[27:33]))

어미소 = []
for m in lines:
    어미소.append(m.split("|")[3].strip())

새식별번호 = []
for b, h in zip(배꾸레, 어미소):
    if b[9:14] in 밑소:
        _new = "%s%s%s" % (b, hex_to_str(_hvoltage)[0], h[7:11],)
        새식별번호.append(_new)
    elif b[9:14] in 후밑:
        _new = "%s%s%s" % (b, hex_to_str(_erice)[0], h[7:11],)
        새식별번호.append(_new)
    else:
        _new = "%s%s%s" % (b, hex_to_str(_ox)[0], h[7:11],)
        새식별번호.append(_new)

###
### test
###

if __name__ == "__main__":
    for 배꾸레 in 새식별번호:
        print(배꾸레)
	        
# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2023년 10월 7일
