# -*- coding: utf-8 -*-

import sys
import json

from lex__ import _get_url, trandom, hex_to_str # custom module

def _(xyz):
    _CI_PROJECT_URL = "https://gitlab.com/soyeomul/Gnus"
    _CI_COMMIT_BRANCH = "MaGnus"
    _FILE_NAME = xyz

    url = "%s/-/raw/%s/%s/%s" % (
        _CI_PROJECT_URL,
        _CI_COMMIT_BRANCH,
        "말씀",
        _FILE_NAME,
    )

    return url


# 제한범위를 넘길경우 교법2장19절 을 자동 반환함 
기본값 = _get_url(_("교법2장19절.txt"))

_mask = 256 # 0x0000 - 0x00ff
_뭉치 = "말씀.json"
_high_voltage = "0x26A1" # U+26A1

fdata = _get_url(_(_뭉치)) # <class 'str'>

def _output(_data):
    dj = json.loads(_data)    
    length = len(dj.keys())
    idx = trandom(length)

    return dj['{0}'.format(idx)]


output = _output(fdata)[1:-1]
if len(output) > _mask:
    output = 기본값

__thanks__ = "%s %s %s" % (
    hex_to_str(_high_voltage)[0] * 3,
    output,
    hex_to_str(_high_voltage)[0] * 3,
)

def _today():
    _ENV = "THANKS_TODAY"
    _VAL = __thanks__

    return "%s%s%s" % (_ENV, "=", _VAL,)


def _fqdn():
    _ENV = "THANKS_FQDN"
    _VAL = "yw.doraji.xyz"

    return "%s%s%s" % (_ENV, "=", _VAL,)


def _domoonsoja():
    _ENV = "THANKS_DOMOONSOJA"
    _VAL = "소여물"

    return "%s%s%s" % (_ENV, "=", _VAL,)


def _mgk():
    _ENV = "THANKS_MGK"
    _VAL = "586b95f5-d09756b1-59ede88732b51479080c53cd3c8d81eb"
    _GOPHER = "@gopher.yw.doraji.xyz" # gopher == nil

    return "%s%s%s%s" % (_ENV, "=", _VAL, _GOPHER,)


if __name__ == "__main__":
    if sys.argv[1] == "--today":
        print(_today())
    if sys.argv[1] == "--fqdn":
        print(_fqdn())
    if sys.argv[1] == "--domoonsoja":
        print(_domoonsoja())
    if sys.argv[1] == "--mgk":
        print(_mgk())

# 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
# 마지막 갱신: 2022년 2월 26일
