#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import re

from lex__ import _get_url # custom module
from jgh import 새식별번호 # custom module

#
# XML 기초 자료
#

jgh_long = [x[2:14] for x in 새식별번호] # 개체전체번호 12자리
jgh_short = [x[9:13] for x in 새식별번호] # 단축번호 4자리
jgh_age = [x[15:18] for x in 새식별번호] # 개체의 개월령 나이
jgh_sex = [x[0:2] for x in 새식별번호] # 개체의 성별
jgh_room = [] # 개체의 우방번호
jgh_ibal = [] # 발정탐지기 착용 유무
jgh_mom = [x[18:] for x in 새식별번호] # 개체의 어미소

_umask = 6 # 우방 명칭의 길이

BRANCH = "karma"
FURL = "https://gitlab.com/soyeomul/hanwoo/-/raw/%s/006gwh.txt" % (BRANCH)
fdata = _get_url(FURL)

lines = fdata.splitlines(False)
lines = [x for x in lines if re.search("^[가-힣][가-힣][가-힣][가-힣]-", x)]

p = re.compile("[0-9][0-9][0-9][0-9]?[0-9]")

def conv(xyz):
    new_xyz = []
    _table = 새식별번호
    for b in xyz:
        if len(b) == 5:
            for h in _table:
                if b in h[9:14]:
                    new_h = h[2:14]
                    new_xyz.append(new_h)
        else:
            for h in _table:
                if b in h[9:13]:
                    new_h = h[2:14]
                    new_xyz.append(new_h)
           

    return new_xyz


# 발정탐지기 착용 유무 계산 시작
def ibal(xyz):
    return xyz[14] == "="

for idx in 새식별번호:
    if ibal(idx) == 1:
        jgh_ibal.append(True)
    else:
        jgh_ibal.append(False)
# 발정탐지기 착용 유무 계산 끝


# 개체 위치 계산 시작
room = []
so = []
for i in lines:
    room.append(i[:_umask])
    so.append(p.findall(i))

soso = list(map(conv, so))
sozip = zip(room, soso)

newt = []
newe = []
def _filter(xyz):
    title = xyz[0][:]
    le = xyz[1]

    if len(le) == 0:
        return
    
    for k in le:
        newt.append(k)
        newe.append(title)

for idx in sozip:
    _filter(idx)

for k in jgh_long:
    for v in list(zip(newt, newe)):
        if k == v[0]:
            jgh_room.append(v[1])
# 개체 위치 계산 끝


# 최종 결과물: 각 한우개체 속성들의 배열 묶음
XML_BASE = list(zip(jgh_short, jgh_long, jgh_sex, jgh_age, jgh_room, jgh_mom, jgh_ibal,))
XML_BASE_IMP = list(zip(새식별번호, jgh_room,)) # 임포트 전용 변수 

###
###
###

if __name__ == "__main__":
    if sys.argv[1] == "--imp":
        for hanwoo in XML_BASE_IMP:
            print(hanwoo)
    if sys.argv[1] == "--env":
        for hanwoo in XML_BASE:
            print(hanwoo)

# 편집: GNU Emacs 27.1 (Debain 11 Bullseye)
# 마지막 갱신: 2022년 4월 23일
