#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 깃랩 작업 보고서
#

import sys
import os
from subprocess import Popen, PIPE, call

import lex__ # custom module


### 전자메일 주요 환경 변수 (사용자 정의)
#
# MAIL_FROM: 보내는이 주소
# RCPT_TO: 받는이 주소
# MAIL_API_KEY: 보내는이 (메일건) API 키
# MAIL_API_ADDR: 보내는이 (메일건) API 주소

MAIL_DOMAIN = os.getenv('THANKS_FQDN') # yw.doraji.xyz
MAIL_USER = os.getenv('THANKS_GOPHER') # soyeomul
MAIL_NAME = os.getenv('THANKS_DOMOONSOJA') # 소여물
MAIL_FROM = "%s <%s@%s>" % (
    MAIL_NAME,
    MAIL_USER,
    MAIL_DOMAIN,
) 
RCPT_TO = os.getenv('GITLAB_USER_EMAIL') # soyeomul@gmail.com
__MGK__ = lex__.se(os.getenv('THANKS_MGK'))[0]
MAIL_API_KEY = lex__.hbh(__MGK__) 
MAIL_API_ADDR = "https://api.mailgun.net/v3/%s/messages" % (
    MAIL_DOMAIN,
)
#
### 전자메일 주요 환경 변수 (사용자 정의) 



### 보고서 제목 및 본문 내용 ###
#
# 내용0. 마지막 커밋 
# 내용1. 커밋 변경점
# 내용2. 빌드 로그 
#
# ... 관련 환경 변수 항목들 ...
#
# - CI_PROJECT_TITLE
#
# - CI_PROJECT_URL
# - CI_COMMIT_SHA
# - CI_COMMIT_MESSAGE

def subject():
    cmd = "LC_TIME=C.UTF-8 date"
    
    _try = Popen(cmd, stdout=PIPE, shell=True)
    ctime = _try.communicate()[0].decode("utf-8").strip()

    PROJECT_TITLE = os.getenv('CI_PROJECT_TITLE')

    return "깃랩 작업 보고서 (%s) -- %s" % (PROJECT_TITLE, ctime)


def commit_url():
    PROJECT_URL = os.getenv('CI_PROJECT_URL')
    COMMIT_SHA = os.getenv('CI_COMMIT_SHA')
    COMMIT_MESSAGE = os.getenv('CI_COMMIT_MESSAGE')

    return PROJECT_URL, COMMIT_MESSAGE, "%s/-/commit/%s" % (
        PROJECT_URL,
        COMMIT_SHA,
    )

    
MAIL_SUBJECT = subject()

MAIL_TEXT = """
... curl 메일건 api 로 보냅니다 ... 

작업장 주소: %s

[0] 마지막 작업: 
===> %s

[1] 작업 변경점: 
===> %s

[2] 빌드 로그: 
===> %s

--
%s
""" % (
    commit_url()[0],
    commit_url()[1],
    commit_url()[2],
    os.getenv('THANKS_BUILD_LOG'),
    os.getenv('THANKS_SIGNATURE'),
)
#
### 보고서 제목 및 본문 내용 ###



curl_mailgun = """\
curl -s --user 'api:%s' %s \
-F from='%s' \
-F to=%s \
-F subject='%s' \
-F text='%s'""" % (
    MAIL_API_KEY,
    MAIL_API_ADDR,
    MAIL_FROM,
    RCPT_TO,
    MAIL_SUBJECT,
    MAIL_TEXT,
)


def _print():
    return curl_mailgun


def send():
    call(curl_mailgun, shell=True)

    return 0



if __name__ == "__main__":
    if sys.argv[1] == "--send":
        sys.exit(send())
    if sys.argv[1] == "--print":
        sys.exit(_print())
    
# 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
# 마지막 갱신: 2021년 10월 16일
