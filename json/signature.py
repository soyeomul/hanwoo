# -*- coding: utf-8 -*-

import os

signature = os.getenv('THANKS_SIGNATURE') # "^고맙습니다 _布德天下_ 감사합니다_^))//"


if __name__ == "__main__":
    print(signature) # 디버깅!!!
    
# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2022년 2월 26일
