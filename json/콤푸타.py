# -*- coding: utf-8 -*-

import sys
import platform
from datetime import datetime, timedelta
from subprocess import Popen, PIPE

#
# 콤푸타 관련 정보
#

def 현재_한국시각():
    UTC_NOW = datetime.utcnow()
    KST = timedelta(hours=9) # +0900

    return UTC_NOW + KST


def _distro():
    cmd = "lsb_release -d -s"
    _try = Popen(cmd, stdout=PIPE, shell=True)
    _output = _try.communicate()[0].decode("utf-8").strip()
    
    return _output


def _uname():
    cmd = "uname -srm"
    _try = Popen(cmd, stdout=PIPE, shell=True)
    _output = _try.communicate()[0].decode("utf-8").strip()

    return _output


갱신 = "마지막 갱신: %s (KST)" % (현재_한국시각())

도구 = "생성도구: " + "Python " + sys.version

운영체제 = "생성도구 운영체제: %s (%s)" % (
    _distro(),
    _uname(),
)

_GITLAB_BUILD_HOSTNAME = "생성도구 콤푸타 이름: " + platform.node() 

대숲농장_셈틀깨비 = "대숲농장 셈틀깨비: %s / %s" % (
    "MT8173 미디어텍 크롬북 (birch/arm64)",
    "크롬OS + 데비안 11 (Bullseye)",
)

# 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
# 마지막 갱신: 2022년 2월 19일
