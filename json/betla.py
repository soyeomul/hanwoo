# -*- coding: utf-8 -*-

from lex__ import hex_to_str # custom module

_ox = "0x1F402" # U+1F402
_rice = "0x1F33E" # U+1F33E

_sox = hex_to_str(_ox)[0]
_srice = hex_to_str(_rice)[0]

betla = """\
%s%s%s 천둥산 박달재를 울고넘는 우리님아 %s%s%s
%s%s%s 물항라 저고리가 굳은비에 젖는구려 %s%s%s
%s%s%s 왕거미 집을짓는 고개마다 구비마다 %s%s%s
%s%s%s 울었소 소리쳤소 이가슴이 터지도록 %s%s%s

%s%s%s 부엉이 우는산골 나를두고 가신님아 %s%s%s
%s%s%s 돌아올 기약이나 성황님께 빌고가소 %s%s%s
%s%s%s 도토리 묵을싸서 허리춤에 달아주며 %s%s%s
%s%s%s 한사코 우는구나 박달재의 금봉이야 %s%s%s

%s%s%s 박달재 하늘고개 울고넘는 눈물고개 %s%s%s
%s%s%s 돌뿌리 걷어차며 돌아서는 이별길아 %s%s%s
%s%s%s 도라지 꽃이피는 고개마다 구비마다 %s%s%s
%s%s%s 금봉아 불러보나 산울림만 외롭구나 %s%s%s""" % (
    _sox, _sox, _sox, _srice, _srice, _srice,
    _sox, _sox, _sox, _srice, _srice, _srice,
    _sox, _sox, _sox, _srice, _srice, _srice,
    _sox, _sox, _sox, _srice, _srice, _srice,
    _sox, _sox, _sox, _srice, _srice, _srice,
    _sox, _sox, _sox, _srice, _srice, _srice,
    _sox, _sox, _sox, _srice, _srice, _srice,
    _sox, _sox, _sox, _srice, _srice, _srice,
    _sox, _sox, _sox, _srice, _srice, _srice,
    _sox, _sox, _sox, _srice, _srice, _srice,
    _sox, _sox, _sox, _srice, _srice, _srice,
    _sox, _sox, _sox, _srice, _srice, _srice,
)    

if __name__ == "__main__":
    print(betla)

# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2021년 4월 10일
