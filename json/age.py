# -*- coding: utf-8 -*-

# 한우 개체 나이
# 기준: 개월령

def _mage(n):
    from datetime import datetime, timedelta

    x = "20" + n[:2]
    y = n[2:4]
    z = n[4:]

    btime = datetime(int(x),int(y),int(z))
    btime = btime + timedelta(days=-1) # 당일출생 예외처리
                                       # 그리고 UTC 대응
    ctime = datetime.now()

    td = str(ctime - btime)
    days = td.split()[0]

    if int(days) < 34:
        mage = "001"
    elif int(days) * 12 % 365 < 26:
        mage = str(int(int(days) * 12 / 365)).zfill(3)
    else:
        mage = str(int(int(days) * 12 / 365 + 1)).zfill(3)

    return mage

# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2021년 4월 21일
