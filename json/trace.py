#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 빌드 로그 추적
#
# 참고문헌:
# <https://docs.gitlab.com/ee/api/jobs.html#get-a-log-file>
#

import sys
import os
from subprocess import Popen, PIPE

import access_token # custom module
 
your_access_token = access_token.ACCESS_TOKEN
your_job_id = os.getenv('THANKS_JOB_ID')
if your_job_id == None:
    your_job_id = os.getenv('CI_JOB_ID')
your_project_id = os.getenv('CI_PROJECT_ID')

_ENV = [
    your_access_token,
    your_project_id,
    your_job_id,
]

x = _ENV[0]
y = _ENV[1]
z = _ENV[2]
    
def get_text(x, y, z):
    url = "https://gitlab.com/api/v4/projects/%s/jobs/%s/trace" % (y, z)
    cmd = "curl -s --location --header 'PRIVATE-TOKEN: %s' '%s'" % (x, url)
    
    _try = Popen(cmd, stdout=PIPE, shell=True)
    output = _try.communicate()[0].decode("utf-8").strip()

    return output, cmd, url


### 임포트 전용 변수 ###
#
trace_url = get_text(x, y, z)[2]
trace_cmd = get_text(x, y, z)[1]
trace_text = get_text(x, y, z)[0] 
#
### 임포트 전용 변수 ###


def _env_url():
    _ENV = "THANKS_BUILD_LOG"
    _VAL = trace_url

    return "%s%s%s" % (_ENV, "=", _VAL,)



if __name__ == "__main__":
    if sys.argv[1] == "--url":
        print(trace_url)
    if sys.argv[1] == "--cmd":
        print(trace_cmd)
    if sys.argv[1] == "--text":
        print(trace_text)
    if sys.argv[1] == "--dotenv":
        print(_env_url())

# 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
# 마지막 갱신: 2021년 10월 15일
