# -*- coding: utf-8 -*-

import json

from lex__ import _get_url # custom module

BRANCH = "karma"
FURL = "https://gitlab.com/soyeomul/hanwoo/-/raw/%s/json/access_token.json" % (BRANCH)
fdata = _get_url(FURL)

dd = json.loads(fdata)
dd = dd['_TOKEN']

_token = []
for k in dd:
    _token.append(chr(dd[k]))

ACCESS_TOKEN = "".join(_token)

# 편집: VIM (Ubuntu 18.04)
# 마지막 갱신: 2022년 4월 23일
