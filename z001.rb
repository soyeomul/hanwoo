#!/usr/bin/ruby2.5
# -*- coding: utf-8 -*-
# Path: /home/soyeomul/hanwoo/z001.rb

# 대숲농장의 수정 현황 기본 자료를 바탕으로
#  설사 예방 백신 접종 일자를 출력해주는 콤푸타 코드.

require "date"

FURL = "https://gitlab.com/soyeomul/hanwoo/raw/master/001sjh.txt"
SPATH = "/tmp/001sjh.txt"
`rm -f #{SPATH}; wget -q -P /tmp #{FURL}`
# 기본자료를 인터넷을 통하여 최신정보로 동기화시킵니다.

FPATH = SPATH

a = `grep NA #{FPATH} | grep -v '^$' | awk -F '|' '{print $1}' | sed -e 's/\s//g'`
b = `grep NA #{FPATH} | grep -v '^$' | awk -F '|' '{print $3}' | sed -e 's/\s//g'`

def _make_oday(n)
  x = "20#{n[0..1]}".to_i
  y = n[2..3].to_i
  z = n[4..5].to_i
    
  oday = DateTime.new(x, y, z, 0, 0, 0, 0.375)

  return oday
end
# 분만 예정일 6자리 숫자를 분석하여 날짜양식으로 형변환
# Thanks to: Brandon Weaver on gmane.comp.lang.ruby.general [2019-01-22]

def the_day_1(n)
  _make_oday(n) - 35
end
# 1차 백신 접종일 계산하기

def the_day_2(n)
  _make_oday(n) - 21
end
# 2차 백신 접종일 계산하기

la = a.split("\n") # 번식우의 단축번호 네자리 숫자
lb = b.split("\n") # 새끼 분만 예정일자
lc = lb.map { |outday| '[' + "1차:" + the_day_1(outday).to_s[2...10] + ']' }
ld = lb.map { |outday| '[' + "2차:" + the_day_2(outday).to_s[2...10] + ']' }
# lc => 1차 백신 접종일자
# ld => 2차 백신 접종일자

data = la.zip(lb,lc,ld)

data.each { |key,value1,value2,value3| puts "#{key} \t #{value1} \t #{value2} \t #{value3}" }

puts '#' + "분만 예정일 기준으로 1차는 5주전, 2차는 3주전 설사 예방 백신 접종을 실시합니다."
puts '#' + "큰 도움 주신 분: 백암목장 김용석 선생님"
puts '#' + "출력시각" + ':' + ' ' + DateTime.now.to_s 

# 실행결과: (텍스트 파일) 
# https://gitlab.com/soyeomul/stuff/raw/master/z001.lst
# 
# 테스트 환경: $ ruby --version
# ruby 2.5.1p57 (2018-03-29 revision 63029) [aarch64-linux-gnu]

# 참고문헌: [1-2]
# [1] 큰틀: https://gitlab.com/soyeomul/hanwoo/raw/master/z000.rb
# [2] 날짜: https://code.i-harness.com/en/q/4ee79e  

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 최초 작성일: 2019년 1월 8일
# 마지막 갱신: 2019년 1월 22일
