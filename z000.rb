#!/usr/bin/ruby1.8
# -*- coding: utf-8 -*-
# Path: /home/soyeomul/hanwoo/z000.rb

# 대숲농장의 한우 전체개체현황 기본 자료를 바탕으로 
#  소의 개월령(나이)을 소 번호, 소 종류와 함께 출력해주는 콤푸타 코드

require "date"

FURL = "https://gitlab.com/soyeomul/hanwoo/raw/master/000jgh.txt"
SPATH = "/tmp/000jgh.txt"
`rm -f #{SPATH}; wget -q -P /tmp #{FURL}`
# 기본자료를 인터넷을 통하여 최신정보로 동기화시킵니다.

FPATH = SPATH

a = `cat #{FPATH} | grep -v '^$' | grep -v ';' | awk -F '|' '{print $1}' | awk -F ' ' '{print $1}'`
b = `cat #{FPATH} | grep -v '^$' | grep -v ';' | awk -F '|' '{print $2}' | sed -e 's/\s//g'`
c = `cat #{FPATH} | grep -v '^$' | grep -v ';' | awk -F '|' '{print $3}' | sed -e 's/\s//g'`
# 우분투 IRC 대화방에서 pchero님께서 알려주신 방법. awk 덕분에 문제를 가까스로 해결했음
# 한국시간으로 대략 2018년 1월 24일경.  ^히어로님~ 고맙습니다 감사합니다_^))// 

la = a.split("\n") # 소의 단축번호 네자리 숫자
lb = b.split("\n") # 소의 종류 (수소/암소/거세/프리마틴)
lc = c.split("\n") # 소의 출생일자

def make_mage(n)
  exp = Regexp.new("^([0-9]{2})([0-9]{2})([0-9]{2})$")

  rs = n.to_s

  ix = exp.match(rs)[1]
  iy = exp.match(rs)[2]
  iz = exp.match(rs)[3]

  sx = "20"+ix.to_s
  x = sx.to_i

  if iy[0].chr == '0'
    y = iy[1].chr.to_i
  else
    y = iy.to_i
  end

  if iz[0].chr == '0'
    z = iz[1].chr.to_i
  else
    z = iz.to_i
  end

  tday = DateTime.now
  bday = DateTime.new(x, y, z, 0, 0, 0, 0.375)
  timedelta = tday - bday

  age = timedelta.to_i

  if age < 31
    mage = 1
  elsif age*12%365 < 26
    mage = age*12/365
  else
    mage = age*12/365+1
  end

  return mage
end
# 출생일자를 입력받아서 개월령(나이)으로 만들어주는 함수
# 태어나서 30일 까지는 1개월령으로 못박고, N개월에서 5일 정도까지는 N개월령으로 간주하며,
#  N개월에서 6일이 경과하면 N+1개월령으로 표기함.
# 5개의 실제 표본으로 축산물 이력정보 조회 서비스의 결과와 비교해봤음. 5개 모두 정확히 맞았음.
# 최종 점검: 2019년 1월 8일. 

lc = lc.map { |birthday| birthday + ' ' + '(' + make_mage(birthday).to_s + "개월령" + ')' }
# 출생일자를 개월령(나이)으로 변환하여 리스트를 재작성하는 과정. 고마워요 map^^^

data = la.zip(lb,lc) 
# 각 리스트를 연관배열(hash)로 합치는 과정
# key => la (소 단축번호)  

data.each { |key,value1,value2| puts "#{key} \t #{value1} \t #{value2}" }
# 연관배열 형식으로 data에 응축된 모든 요소들을 보기 좋게 출력하는 과정 

puts "#전체두수: " + la.length.to_s
puts "#출력시각: " + DateTime.now.to_s 
# 개월령(나이)은 상대적인 값이므로 비교값인 출력시각을 항상 함께 표기해줘야함

# 실행결과: (텍스트 파일) 
# https://gitlab.com/soyeomul/stuff/raw/master/z000.lst
# 
# 테스트 환경: $ ruby --version
# ruby 1.8.7 (2011-06-30 patchlevel 352) [i686-linux]
# ruby 2.5.1p57 (2018-03-29 revision 63029) [aarch64-linux-gnu]

# 참고문헌: [1-3]
# [1] https://ruby-doc.org/core-1.8.7/Array.html#method-i-zip
# [2] https://opentutorials.org/module/517/4589
# [3] 축산물 이력정보 조회 서비스 (개월령 계산 함수 검산할때 활용했음)

# 편집: Emacs 23.3 (Ubuntu 12.04)
# 최초 작성일: 2018년 1월 24일
# 마지막 갱신: 2019년 5월 14일
