#!/usr/bin/python3
# -*- coding: utf-8 -*-

# z001.rb 와 같은 역할을 합니다.
# 대숲농장의 수정 현황 기본 자료를 바탕으로,
#  1차, 2차 백신접종일자를 계산해주는 콤푸타 코드입니다.

import sys
import subprocess
from datetime import datetime, timedelta

BRANCH = "karma"
FURL = "https://gitlab.com/soyeomul/hanwoo/-/raw/%s/001sjh.txt" % (BRANCH)
PRE_CMD = "rm -f /tmp/001sjh*"
CMD_1ST = "grep NA /tmp/001sjh.txt | grep -v '^$' | awk -F '|' '{print $1}' | sed -e 's/\s//g' > /tmp/001sjh.1st"
CMD_2ND = "grep NA /tmp/001sjh.txt | grep -v '^$' | awk -F '|' '{print $3}' | sed -e 's/\s//g' > /tmp/001sjh.2nd"
# 쉘 명령어들을 이용하기 위하여 긴 문법을 미리 기록해 둠 

subprocess.call(PRE_CMD + "; " + "wget -4 -q -P /tmp " + FURL, shell=True)
# /tmp 에 있던 과거 파일들을 지우고 새파일을 인터넷에서 받는과정

subprocess.call(CMD_1ST, shell=True)
fa = open("/tmp/001sjh.1st", "r"); ra = fa.read(); la = ra.split(); fa.close()
# 번식우 단축번호를 리스트(la)로 집어넣는 작업

subprocess.call(CMD_2ND, shell=True)
fb = open("/tmp/001sjh.2nd", "r"); rb = fb.read(); lb = rb.split(); fb.close()
# 새끼 분만 예정일을 리스트(lb)로 집어넣는 작업

def make_outday(n):
    x = "20" + n[0:2]
    y = n[2:4]
    z = n[4:]
    
    outday = datetime(int(x),int(y),int(z))
  
    return outday
# 6자리 숫자를 분석하여 실제 분만 예정일을 계산하는 과정

def the_day_1(n):
    result = make_outday(n) + timedelta(days=-35)
    the_day = str(result)[0:10]
    
    return the_day
# 1차 백신 접종일 계산하기

def the_day_2(n):
    result = make_outday(n) + timedelta(days=-21)
    the_day = str(result)[0:10]
    
    return the_day
# 2차 백신 접종일 계산하기

lc = list(map(lambda outday: the_day_1(outday), lb))
ld = list(map(lambda outday: the_day_2(outday), lb))
# lb 를 바탕으로 lc 와 ld 를 재작성합니다.
# Ruby 의 map 처럼 재구성된 리스트를 반환해줍니다.
# 고마워요 map~ 감사해요 lambda~

for v1,v2,v3,v4 in zip(la,lb,lc,ld):
    print(v1,'\t',v2,'\t',"1차:"+v3,'\t',"2차:"+v4)
# la => 번식우 단축번호
# lb => 분만 예정일자
# lc => 1차 백신 접종일
# ld => 2차 백신 접종일
# 계산되어진 응축된 정보들을 보기좋게 출력하기

print("#분만 예정일 기준으로 1차는 5주전, 2차는 3주전에 설사 예방 백신 접종을 실시합니다.")
print("#큰 도움 주신분: 백암목장 김용석 선생님")
print("#출력시각:", datetime.now())
print("#sys.hexversion:", hex(sys.hexversion))
# 시간과 날짜 정보는 늘 상대적인 값이므로, 해당정보를 출력한 시각이 매우 중요합니다.

# 본 코드는 우분투 리눅스 18.04, 파이썬 3.6.7 환경에서 만들어졌습니다.

# 참고문헌:
# [1] https://yuddomack.tistory.com/entry/%ED%8C%8C%EC%9D%B4%EC%8D%AC-datetime-%EB%82%A0%EC%A7%9C-%EA%B3%84%EC%82%B0
# [2] https://stackoverflow.com/questions/4267206/does-ruby-have-a-zip-function-like-pythons
# [3] https://docs.python.org/2/library/subprocess.html
# [4] https://gitlab.com/soyeomul/hanwoo/raw/master/z001.rb

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 최초 작성일: 2019년 1월 24일
# 마지막 갱신: 2023년 2월 12일
