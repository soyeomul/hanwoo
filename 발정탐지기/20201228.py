# -*- coding: utf-8 -*-

###
### 발정탐지기 정비
###

import sys
sys.path.insert(0, "/home/soyeomul/__system__")

from lex__ import _get_url # custom module

### 000jgh.txt

src1 = "https://gitlab.com/soyeomul/hanwoo/-/raw/master/000jgh.txt"
f1 = _get_url(src1)
data1 = f1.splitlines(False)

n1 = []
for k in data1:
    if "002" in k:
        n1.append(k)

r1 =[]
for v in n1:
    e = v.split(" ")[1]
    r1.append(e[1:-1])

### 002ash.txt

src2 = "https://gitlab.com/soyeomul/hanwoo/-/raw/master/002ash.txt"
f2 = _get_url(src2)
data2 = f2.splitlines(False)

n2 = []
for k in data2:
    if k.startswith("F") and "4278" in k:
        n2.append(k) 

r2_1 = []
r2_2 = []
for k in n2:
    r2_1.append(k.split(" ")[0][1:])
    r2_2.append(k.split(" ")[4])

### 전체 재정렬

r2_1_1 = []
for i in r2_1:
    for j in range(len(r1)):
        if i in r1[j]:
            r2_1_1.append(r1[j])

t = list(zip(r2_2, r2_1_1))
t.sort(key=lambda t: t[0])

for k in t:
    print(k)

# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 최초 작성일: 2020년 12월 28일
# 마지막 갱신: 2020년 12월 29일
