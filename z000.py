#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# z000.rb 의 파이썬 구현
# 테스트: Debian 11 Bullseye

# 참고문헌:
# [1] https://gitlab.com/soyeomul/hanwoo/blob/master/z000.rb
# [2] https://kldp.org/comment/630681#comment-630681
# [3] https://stackoverflow.com/questions/38254795/using-curl-in-popen-in-python

import sys
import subprocess
from datetime import datetime, timedelta

BRANCH = "karma"
FURL = "https://gitlab.com/soyeomul/hanwoo/-/raw/%s/000jgh.txt" % (BRANCH)

p = subprocess.Popen("curl -s {} | grep ^[0-9][0-9][0-9][0-9]".format(FURL), \
                     stdout=subprocess.PIPE, shell=True)
ptr = p.communicate()[0].decode("utf-8").strip()

lines = ptr.split("\n")

v0 = []
for x in lines:
    v0.append((x.split("|")[0])[0:4])
cow_numbers = v0

v1 = []
for x in lines:
    v1.append((x.split("|")[1]).strip())
cow_sex = v1

v2 = []
for x in lines:
    v2.append((x.split("|")[2]).strip())
age_string = v2

def make_birth(n):
    x = "20" + n[0:2]
    y = n[2:4]
    z = n[4:]
    birth = datetime(int(x),int(y),int(z))
    
    return birth

def make_mage(n):
    ctime = datetime.now()
    if str(ctime)[0:10] == str(make_birth(n))[0:10]: # 당일 출생 예외 처리
        btime = make_birth(n) + timedelta(days=-1)
    else:
        btime = make_birth(n)
    td = str(ctime - btime)
    days = td.split()[0]

    if int(days) < 100:
        """
        오늘부터 91일부터 99일 사이로 젖때는 시기로 정한다.
        깐돌이들의 잦은 설사가 큰 이유이다.
        젖먹이 사료만 잘 급여하면 문제없다.
        또한 번식우의 수정 주기도 고려하여 결정했음.
        정책변경일자: 20190923 (추분)
        """
        mage = str(days) + " (일령)" # 100일 이전까지의 깐돌이는 일수 붙임
    elif int(days) * 12 % 365 < 26: # 100일 부터는 개월수 붙임
        mage = str(int(int(days) * 12 / 365)) + " (개월령)"
    else:
        mage = str(int(int(days) * 12 / 365 + 1)) + " (개월령)"

    return mage

mages = list(map(lambda xyz: xyz + " " + make_mage(xyz), \
                 age_string))

for x, y, z in zip(cow_numbers, cow_sex, mages):
    print(x, "\t", y, "\t", z)

print("#전체두수:", len(cow_numbers))
print("#출력시각:", datetime.now())
print("#sys.hexversion:", hex(sys.hexversion))

#편집: GNU Emacs 27.1 (Debian 11 Bullseye)
#마지막 갱신: 2024년 4월 16일
